const throttleQueue = {};
Object.assign(throttleQueue, {
  jobQ: [],
  scheduler: null,

  /**
   * Queue a function, and a parameter: push(func, {para: 'noid'});
   * Queue a function not needing a parameter: push(func, null, true);
   * Parameter can by of any type, only 1 parameter spot is available.
   * Multiple parameters should be wrapped in an object.
   * @param {function} function/job
   * @param {object} parameter
   * @param {boolean} needsNoParameter
   * @memberof Queue
   */
  push: (job, parameter, needsNoParameter) => {
    if (job && parameter) {
      throttleQueue.jobQ.push({
        job,
        parameter,
      });
    } else if (job && needsNoParameter) {
      throttleQueue.jobQ.push({
        job,
      });
    }
    throttleQueue.startQueue();
  },
  /**
   * Manually cycles one job call
   * Can be called outside of FQ if need be
   * @memberof throttleQueue
   */
  cycleOnce: () => {
    if (throttleQueue.jobQ.length > 0) {
      const jobWrapper = throttleQueue.jobQ[0];

      if (jobWrapper.job && jobWrapper.parameter) {
        jobWrapper.job(jobWrapper.parameter);
      } else if (jobWrapper.job) {
        jobWrapper.job();
      }

      throttleQueue.jobQ.splice(0, 1);
    } else {
      throttleQueue.scheduler = null;
    }
  },
  /**
   * Start the automated queue
   * @memberof throttleQueue
   */
  startQueue: () => {
    if (throttleQueue.scheduler === null) {
      throttleQueue.scheduler = setInterval(throttleQueue.cycleOnce, 500);
    }
  },
});

module.exports = throttleQueue;
